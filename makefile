TARGET=rpi_temp
#此处用于定义编译器，编译的时候可以使用
# make CC=aarch64-none-linux-gnu-gcc，来定义交叉编译用的编译器
CC=gcc
SRC=$(wildcard src/*.c)
INC=./inc
#此处用于定义搜索gpio.d头文件的位置
EXT_INC=../libgpiod-1.6.4/include
#此处用于定义引用的libgpiod.so
LIB=gpiod
#此处用于搜索libgpiod.so的位置
LDFLAGS=-L../libgpiod-1.6.4/INSTALL/lib
#把src变量里所有后缀为*.c的文件替换成*.o
OBJ=$(patsubst %.c,./obj/%.o,$(notdir $(SRC)))
CFLAGS=-I$(INC) -I$(EXT_INC) -O2 -DDEBUG

all:$(TARGET)
$(TARGET):$(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS) -l$(LIB)
obj/%.o:src/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY:clean
ifeq ($(OS),Windows_NT)
RM=cmd /c del
FixPath=$(subst /,\,$1)
else
RM=rm -f
FixPath=$1
endif
clean:
	$(RM) $(call FixPath,obj/main.o)
	$(RM) $(call FixPath,./rpi_temp)