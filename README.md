# 树莓派 OpenWRT系统控制5V风扇自动开关脚本

1. 脚本是使用shell代码直接编写的（不需要编译）
2. 使用系统自动的crontab来实现每隔15分钟对cpu温度进行检测，并控制风扇是否开启和关闭

# 要用到的配件
 1. 5V 10mm厚度的风扇一个
 2. S8050一个（类型为N沟）
 3. 杜邦线3条

# 连接示意图

![rpi_fan_control](./gpio-fan.png)


# 脚本安装方法
 1. 下载源代码到任意目录，并复制其中的rpifanctl.*文件到/etc/cron.d目录（如果目录不存在，就需要自己创建它）
 2. 把需要用到的文件复制到/etc/cron.d 目录后，再进行小量修改，以适应你自己的环境

# 默认配置解释
rpifanctl.cronfile
```
15 * * * * /etc/cron.d/rpifanctl.sh 12 48 40
```
 1. 此处定义的时间为第15分钟进行一次温度检测，用到的gpio接口为12,程序运行目录为/etc/cron.d
 2. 设置风扇开启温度为48度，风扇关闭温度为40度

# 如何加载计划任务
 直接使用crontab就可以了，命令如下
```
crontab -f /etc/cron.d/rpifanctl.cronfile
service cron reload
```

# 使用gpiod来控制风扇

## 在非树莓派平台上台编译
    1. 交叉编译libgpiod-1.6.4
    2. 交叉编译rpi_temp项目
        2.1 需要用到aarch64-none-linux-gnu toolchain
        2.2 安装方法，是用pscp把编译得到的rpi_temp复制到rpi的任意目录即可，如果是持久化，需要安装到 /usr/bin/目录
    3. 配置好rpi_temp后，使用方法
        3.1 使用方法(line_num为要用到的gpio端口号，min_temp关闭风扇温度，max_temp打开风扇温度)
            /usr/bin/rpi_temp --line_num=12 --min_temp=45 --max_temp=51

## 在树莓派平台上面编译
    1. 安装libgpiod-1.6.4,gpiod-tools
    2. 安装gcc toolchains
    3. 使用 make直接编译
    4. 安装，需要手动复制到自己需要的目录
    5. 使用方法(line_num为要用到的gpio端口号，min_temp关闭风扇温度，max_temp打开风扇温度)
            /usr/bin/rpi_temp --line_num=12 --min_temp=45 --max_temp=51